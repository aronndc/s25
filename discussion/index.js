console.log("Hello World!");

/* JSON Objects
	- JSON stands for JavaScript Object Notation
	- JSON is also used for other programming languages hence the name JavaScript

	Syntax:
		{
			"propertyA": "valueA",
			"propertyB": "valueB"
		}
*/

// JSON Object

/*
{
		"City": "Makati City",
		"Province": "Metro Manila",
		"Country": "Philippines"
}

*/

// JSON Array

/*"cities" = [
	{'city': "Makati City", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Manila City", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Pasay City", "province": "Metro Manila", "country": "Philippines"},
]*/

// JSON Methods
/*
	The JSON Object contains methods for parsing and converting data into stringified JSON
*/

// onverting Data into Stringified JSON
let batchesArr = [
	{batchname: "Batch 197"},
	{batchname: "Batch 198"}
]

console.log(batchesArr);

// The stringify method is used to caonvert JS Objects into string
// We are doing this before sending the data, we convert an array or an object tp its string equivalent.

console.log("Result from stringify method:")
console.log(JSON.stringify(batchesArr));

console.log(" ");

let userProfile = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city: "Manila",
		Region: "Metro Manila",
		Country: "Philippines"
	}
})

console.log("Result from stringify method (object).");
console.log(userProfile);

console.log(" ");

// User Details
/*let firstName = prompt("What is your first name?");
let lastName = prompt("What is your last name?");
let age = prompt("What is your age?")
let address = {
	city: prompt("Which city do you live in?"),
	Country: prompt("Which country does your city address belong to?")
}

let userData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})

console.log(userData);*/

console.log(" ");

// COnvert stringified JSON into JS Objects
// JSON.parse()

let batchesJSON = `[
	{
		"batchName": "Batch 197"
	},
	{
		"batchName": "Batch 198"
	}
]`

console.log(batchesJSON);

// Upon receiving data, the JSON text can be converted into JS objects so that we can use it in our program.

console.log("Result from parse method: ");
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{
	"name": "Ivy",
	"age": 18,
	"address": {
		"city": "Makati City",
		"country": "Philippines"
	}
}`

console.log(stringifiedObject);
console.log("Result from parse method (object):");
console.log(JSON.parse(stringifiedObject));
